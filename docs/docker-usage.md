# Scenic-core Docker usage

Scenic-core can be run from a pre-built Docker binary image available from registry.gitlab.com/sat-mtl/tools/scenic/scenic-core.

Image uses several host services. You need to setup your Xubuntu 22.04 host to offer following services.

  - X server on DISPLAY=:0
  - avahi-daemon service (to allow libndi to browse and announce NDI streams on local subnet)
  - Jack2 server already started (an example using systemd user unit service is provided in this guide)
  - Real-time and memlock permissions configured (automatically configured by installing `jackd2` package)
  - Local user with UID 1000 part of audio and video groups
  - MIDI kernel module snd_seq (normally installed on desktop, if not install `alsa-base` package)
  - some Video4Linux compatible device (Scenic is tested with Magewell Pro Capture, USB Capture and Logitech C920 devices)

If you want to enable Scenic support for hardware video encoders and decoders, you will need host to have an Nvidia GPU from at least 2012. Oldest cards supporting NVENC begin with Kepler GK104 (i.e. Geforce GT 630 or Quadro K420). You will need Nvidia proprietary driver along with NVIDIA Container Toolkit ([nvidia-docker2](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html) package) installed, and Docker  on host Docker service.

## Setup host for scenic-core

### Disable screen blanking

```bash
mkdir -p ~/.config/autostart
cat << EOF > ~/.config/autostart/disable-screen-blanking.sh
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/blank-on-ac -s 0
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/blank-on-battery -s 0
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/dpms-enabled -s false
xfce4-screensaver-command --exit
EOF
chmod +x ~/.config/autostart/disable-screen-blanking.sh
```

### Install Docker

Install Docker and add your user to `docker` group.

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo apt install docker-compose-plugin
sudo /sbin/usermod -aG docker $USER
```

### Install Jack

Install Jack2, configure real-time priority permissions and add user to audio group.

During Jack's installation, you’ll have a question asking you to enable real-time priority. Accept by answering "Yes".

```bash
sudo apt install -y jackd2
sudo /sbin/usermod -aG audio $USER
```

### Define Jack service

Add a `systemd` user service definition file:

```bash
mkdir -p ~/.config/systemd/user
wget -O ~/.config/systemd/user/jack@.service https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/raw/master/config/jack@.service
```

### Find audio interface name

List audio interfaces:

```bash
aplay -L | grep "^hw:CARD="
```

Example listing for Behringer XR18. Take note of the device name “X18XR18” for next step.

```
hw:CARD=X18XR18,DEV=0
```

Example listing for M-Audio M-Track Eight. Take note of the device name “Eight” for next step.

```
hw:CARD=Eight,DEV=0
```

### Configure and start Jack service

**Modify the `DEVICE="hw:X18XR18"` line with your audio interface device name, as found in the last step.**

```bash
mkdir -p ~/.config/jack
cat << EOF > ~/.config/jack/scenicbox.conf
# Start using "systemctl --user start jack@scenicbox"
# Follow logs of jackd2 with "journalctl --user --follow --unit jack@scenicbox"
# The name of the JACK server
JACK_DEFAULT_SERVER="default"
# Options to JACK (e.g. -m, -n, -p, -r, -P, -t, -C, -u, -v)
JACK_OPTIONS="--realtime"
# Audio backend (e.g. alsa, dummy, firewire, netone, oss, portaudio)
DRIVER="alsa"
# Device name (used by the audio backend)
DEVICE="hw:X18XR18,0"
# Specific settings for the audio backend in use
DRIVER_SETTINGS="--rate 48000 --period 1024 --nperiods 3 --duplex"
EOF
```

Enable and start Jack service:

```bash
systemctl --user daemon-reload
systemctl --user enable jack@scenicbox
systemctl --user restart jack@scenicbox
```

Verify if Jack service started without any errors:

```bash
systemctl --user status jack@scenicbox
```

### Logout of desktop environment session

To apply membership to the `audio` and `docker` groups, you need to logout of your current desktop environment session.

When using graphical XFCE session, click on the XFCE start menu then click on the "Log Out" button. Then, log back in.

## Setup NVIDIA support

If you are using an NVIDIA GPU and want to use the NVENC hardware video encoder, you'll need to install the NVIDIA proprietary driver along with NVIDIA's container runtime.

### Install NVIDIA proprietary driver

Scenic is currently tested using version 510. We recommend installing this version.

```bash
sudo apt install -y nvidia-driver-510
```

Reboot your computer to activate the new graphical driver.

```bash
sudo reboot
```

### Install NVIDIA Container Toolkit

To allow Scenic's Docker container to use the NVENC hardware encoder, you'll need install and configure `nvidia-container-runtime`.

```bash
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
      && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
            sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
            sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
sudo apt update -y
sudo apt install -y nvidia-docker2
```

### Enable NVIDIA container runtime

Modify the Docker configuration file to enable the NVIDIA container runtime:

```bash
sudo tee /etc/docker/daemon.json > /dev/null << EOL
{
    "storage-driver": "overlay2",
    "runtimes": {
        "nvidia": {
            "path": "nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}
EOL
```

Restart Docker to apply the new configuration:

```bash
sudo systemctl restart docker
```

## Setup Magewell capture cards drivers

### Install Magewell Pro Capture driver

```bash
sudo apt-get -y install --no-install-recommends make gcc dkms
wget https://www.magewell.com/files/drivers/ProCaptureForLinux_4328.tar.gz
tar xvf ProCaptureForLinux_4328.tar.gz
cd ProCaptureForLinux_4328
sudo ./dkms-install.sh
```

### Install Magewell Eco Capture driver

```bash
sudo apt-get -y install --no-install-recommends make gcc
wget https://www.magewell.com/files/drivers/EcoCaptureForLinuxX86_1.4.227.tar.gz
tar xvf EcoCaptureForLinuxX86_1.4.227.tar.gz
cd EcoCaptureForLinuxX86_1.4.227
sudo ./install.sh
```

## Configure Scenic Station contacts, bundles and menus

Create a configuration directory:

```bash
mkdir -p ~/.config/sat
```

Copy Scenic's contact file to `~/.config/sat/contacts.json`.

Copy Scenic's bundles file to `~/.config/sat/bundles.json`.

Copy Scenic's menus file to `~/.config/sat/menus.json`.

## Launch latest stable version of Scenic

Fetch the Docker Compose file:

```bash
wget https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/raw/master/docker-compose.yml
```

You need to choose which profile to launch scenic-core, either using Nvidia GPU or generic GPUs (Intel/AMD/QXL).

Launching with Nvidia support :

```bash
PUID=$UID PGID=$GID docker compose --profile nvidia up -d
```

Launching for other GPU support:

```bash
PUID=$UID PGID=$GID docker compose --profile generic up -d
```

## Launch latest development version of Scenic

Fetch Docker Compose file:

```bash
wget https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/raw/develop/docker-compose.yml
```

You need to choose with which profile you want to launch `scenic-core`, either using Nvidia GPU or generic GPUs (Intel/AMD/QXL).

Launching with Nvidia support :

```bash
SCENIC_CORE_TAG=develop SCENIC_TAG=develop PUID=$UID PGID=$GID docker compose --profile nvidia up -d
```

Launching for other GPU support:

```bash
SCENIC_CORE_TAG=develop SCENIC_TAG=develop PUID=$UID PGID=$GID docker compose --profile generic up -d
```

## Open Scenic Web GUI

To open a Scenic instance deployed on your local machine, visit `http://localhost:8080` on a web browser (Scenic is tested on Chrome-based browsers).

To open a Scenic instance deployed on a remote machine, visit `http://IP-address:8080/?endpoint=IP-address:8000`

## Stopping Scenic

Stopping with Nvidia support :

```bash
docker compose --profile nvidia down
```

Stopping for other GPU support:

```bash
docker compose --profile generic down
```


## Debug Scenic deployment

Consult `scenic-core`'s logs:

```bash
docker logs scenic-core
```

Restart Scenic Core:

```bash
docker restart scenic-core
```

Update images for `master` tag:

```bash
docker compose pull
```

Update images for specific tags:

```bash
SCENIC_CORE_TAG=develop SCENIC_TAG=develop docker compose pull
```

