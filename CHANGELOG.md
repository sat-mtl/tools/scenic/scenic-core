Release Notes
===================

scenic-core 4.2.0 (2024-07-23)
----------------------------------

* Add test contact list ([!194](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/194))
* Fix buildx on TLS dind ([!193](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/193))
* Update ndi2shmdata to v0.7.0 ([!192](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/192))
* Update GStreamer from 1.22.6 to 1.24.2 ([!191](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/191))
* Complete Switcher.IO integration ([!188](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/188))

scenic-core 4.1.4 (2024-03-20)
----------------------------------

* 📦 Update switcher to 3.5.4
* 📦 Update shmdata to 1.3.74
* ✨ Add custom rswebrtc plugin ([!186](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/186))
* ✨ Add Switcher Pipesplint plugin ([!184](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/184))
* 👷 Fix missing cerbero tag for build job and disable buildx cache ([!183](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/183))
* ⬆️ Add GStreamer 1.22.6 from custom Cerbero build ([!180](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/180))

scenic-core 4.1.3 (2023-12-05)
----------------------------------

* 📦️ Update switcher to 3.5.3

scenic-core 4.1.2 (2023-10-25)
----------------------------------

* 📦️ Update switcher to 3.5.2

scenic-core 4.1.1 (2023-08-21)
----------------------------------

* 📦️ Update switcher to 3.5.1
* 🐛 Fix crash with more than 7 JACK quiddities ([!175](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/175))

scenic-core 4.1.0 (2023-06-15)
----------------------------------

* 📦️ Update switcher to 3.5.0
* 💚 map default switch builds on the develop branch
* 🔥 remove unsynched gitlab templates
* 👷 Add SWITCHER_TAG variable to CI build system

scenic-core 4.1.0-alpha (2023-03-28)
----------------------------------

* 📦️ Update switcher to 3.4.0
* 📦️ Update shmdata to 1.3.66
* 🚑️ Clean stray shmdata paths at startup ([!169](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/169))
* 🗑️ SwitcherIO migration ([!155](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/155))
* 📝 Allow for core dump when running in Docker ([!168](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/155))

scenic-core 4.0.10 (2023-01-26)
----------------------------------

* 🚑️ Fix docker and docker-compose configurations
* 🐛 Fix Jack systemd unit file space escape syntax ([!165](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/165))
* 📝 Add indication to use a more recent switcher and to install g++-8 ([!163](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/163))
* 📝 Rework Scenic deployment documentation ([!164](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/164))
* 🐛 Fix docker compose scenic-web port ([!162](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/162))
* 🐛 Fix dynamic passthrough of v4l2 devices ([!161](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/161))
* 🐛 Restart unless stopped ([!160](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/160))
* 🌱 Add scenic-core CI contacts ([!159](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/159))
* ✨ Add PORT variable in Docker image and allow for UID mapping ([!157](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/157))
* 📝 Add scenic command usage output to README ([!156](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/156))
* ✨ Passthrough any devices to scenic-core image ([!154](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/154))

scenic-core 4.0.7 (2022-09-16)
----------------------------------

* ⏪ Revert buildx and push to support multiarch ([!152](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/152))
* 💚 Fix new GitLab paths ([!151](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/151))
* 💚 Fix base images tags to use CI_COMMIT_REF_NAME ([!150](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/150))

ScenicCore 4.0.6 (2022-08-17)
----------------------------------

Update of all docker images in order add easy to use environment of top of `ScenicCore`

* 🐛 Fix path typo in release-scenic-core-runtime-deps-docker-image ([!146](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/146))
* 📦 Update Docker image build ([!145](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/145))

Scenic Core 4.0.5 (2022-02-02)
----------------------------------
I lied

- 🔥 Remove thumbnails property change debug log

Scenic Core 4.0.4 (2022-01-14)
----------------------------------

🚨 FINAL RELEASE (I hope) with logging capacity

* 🐛 Fix log messages
* ♻ Fix date format for better organization of the log files
* 🔊 Write scenic logs in a file
* 🔊 Add scenic-core/switcher logs to a file
* 🔊 Create a generic function to get the path of the log folder(s)
* 📝 Add minimal documentation for the logging feature

Scenic Core 4.0.2 (2021-08-11)
----------------------------------

* 🐛 Ensure that the save folder exists

Scenic Core 4.0.1 (2021-04-06)
----------------------------------

* ⬆ Upgrade socket.io to 3.1.0
* 🐛 Fix argument parsing in file.read
* 🔥 Remove logic related to configuration files

Scenic Core 4.0.0 (2020-12-08)
----------------------------------

🚨 Heads-up! This new version is not compatible with `Scenic 3.3.5`.

* ⬆ Update Node-Switcher version
* ⬆ Upgrade Node-Switcher to 3.1.2
* 🐛 Fix the path resolution
* 🔥 Remove deprecated subscription and masterSocket logic
* 🔒 Fix Chrome warning about sameSite cookies
* 🔥 Remove Timelapse logic
* 🔥 Remove quiddity.userData.set command
* 🔥 Apply general code clean up
* ✨ Add WS route for fetching contact list
* 🐛 Comment thumbnail parsing when a shmdata is grafted
* ✨ Set nickname on quiddity creation

Scenic Core 3.3.5 (2020-07-14)
----------------------------------

* 🔧 Add debug-gdb recipe in the Makefile

Scenic Core 3.3.4-rc1 (2020-03-31)
---------------------------------

* 📌 Update version to 3.3.4-rc1

Scenic Core 3.3.4 (2020-03-26)
---------------------------------

* 🚑 Release Switcher 2.1 hotfix for 3.3.4
* ✨ Add network routes in order to check network statuses
* ⬆️ Upgrade node-switcher version
* ✨ Add new websocket API for loading bundles

Scenic Core 3.3.3 (2020-02-06)
---------------------------------

* 📝 Update documentation
* 🐛 Make thumbnail API safe again
* ⬆️ Update node-switcher to 3.0.3
* ⚡️ Polish API for thumbnail

Scenic Core 3.3.2 (2020-01-07)
---------------------------------

* 🚑 Update Node Switcher with hotfixes for namespace refactoring
* Added new IOSocket calls for grafted and pruned signals

Scenic Core 3.3.1 (2019-11-25)
---------------------------------

* :pencil: Update documentation for Scenic 3.3.1
* Add quiddity.userData.get.js command
* Fix a rare case where install can fail if link in /usr/lib is broken

Scenic-Core 3.3.0 (2019-10-11)
-------------------------

* Added support for NewTek NDI®
* Added full Docker support
* Added CI task to deploy Scenic Core's Docker container
* Added default categories when no menu configuration is present
* Added hidden quiddity "ndiSniffer" to detect NDI® streams on the network
* Added Make recipes to install `scenic` (and `scenic-debug`) globally
* Added short alias for CLI's help and version
* Fixed broken Scenic User Manual link
* Fixed broken SOAP control
* Fixed buggy session loading
* Fixed installation of node-switcher from npm packages
* Fixed shutdown signal handling
* Fixed Switcher history reset logic
* Fixed timestamps in `activity.log`
* Fixed a bug where a MIDI destination could not be started after changing the MIDI device
* Fixed a bug where the Settings panel would not open automatically after creating a source
* Fixed a bug where the shmdata path was not updated in the UI after loading a session
* Fixed a bug where Scenic would crash when the `.scenic` folder was not present in the user's home folder
* Refactored all startup modules with async functions
* Refactored Server logic with folder checking
* Updated NodeJS 8 to NodeJS 10

Scenic Core 3.2.0 (2019-05-15)
-------------------------

Added documentation about the development process
Added instructions on how to launch Scenic with GDB
Added back pixel format information for video sources in the info panel
Started using static middleware to serve static files in the assets directory
Updated install instructions
Refactored the CI pipeline
Removed the chat completely
Prevented some properties from being displayed for the nvenc, jacksrc and jacksink quiddities
Fixed a bug where SIP connections would stop working after a quiddity's property changed

Scenic Core 3.1.3 (2019-01-24)
-------------------------

Fix faulty SOAP port configuration

Scenic Core 3.1.2 (2019-01-22)
-------------------------

Remove broken JackdSanitizer class and associated logic

Scenic Core 3.1.1 (2018-12-17)
-------------------------

Add semantic version check in CI pipeline
Add linting check in CI pipeline

Scenic Core 3.1.0 (2018-12-13)
-------------------------

Updated and refactored documentation and README
Added CONTRIBUTING, CODEOWNERS, AUTHORS, LICENSE and CODE_OF_CONDUCT files
Packaged Scenic Core for NPM and added a publishing script in the CI: all new versions of Scenic Core will also be pushed to NPM
Added a JackSanitizer class, to manage and kill Jack servers when jacksrc and jacksink quiddities are created
Added a default config for STUN/TURN servers
Added configurations to prevent certain methods and properties of quiddities to be fetched and displayed by the Scenic UI
Updated Sinon from v1.17.3 to v7.1.1 and Sinon-Chai from v2.8.0 to v3.3.0
Refactored Makefile and CI pipeline
Extracted Node add-on for Switcher from this repo and moved it to the brand new node-switcher repo
Fixed failing unit tests
Fixed a bug where creating a JACK quiddity would make the server crash
Fixed broken authentication feature for Scenic remote control
Fixed erroneous timestamps in the log files

Scenic Core 3.0.0 (2018-07-23)
-------------------------

New development cycle started!
Separated the Scenic server-side from the client-side: they are now 2 different projects with a brand new repo each
Refactored and reformatted some of the code
Cleaned and updated dependencies
Completely removed i18next from the server; internationalization will now be handled by the client exclusively

Scenic 2.10.2 (2017-11-27)
-------------------------

Fixed scenic quit when changing language or pressing f5

Scenic 2.10.1 (2017-10-27)
-------------------------

Refactored switcher log access
Fixed css bug on contact actions transition when chat is disabled
Hide vertical scrollbar in scene menu
Updated version of socketio, changed ping timeout to 60 sec
Added a scrollbar when there are more than 10 scenes

Scenic 2.10.0 (2017-09-01)
-------------------------

Add a Chat !!
User can talk directly to each other or create group of discussions

Scenic 2.9.21 (2017-08-14)
-------------------------

Fixed bug when changing the name of a quiddity from a remote control
Fixed bug when adding a contact from a remote control
Add the possibility to pin a scene
Fixed bug when renaming a file

Scenic 2.9.20 (2017-07-19)
-------------------------

Optimization of the scene system
Fixed some bugs synchronization with the remote control
Fixed various bugs around scenes
Possibility to put a scene off in the connections screen
Add some function to rename a quiddity

Scenic 2.9.19 (2017-07-03)
-------------------------

Fixed the header of sources and destinations when scrolling the matrix
Add the possibility to block all contact during a call
STUN, TURN server can be saved in a config file
Fixed various bugs around scenes and matrix

Scenic 2.9.18 (2017-06-07)
-------------------------

Remove the system of groups for the Matrix
Add the possibility to create Scenes that saved the connections for each matrix
Add a connections page to switch between Scenes
Add the possibility to duplicate a scene

Scenic 2.9.17 (2017-04-12)
-------------------------

        ******************************
        ****** Official Release ******
        ******************************

Fixed bug on shmdata connections with rtmp quiddity.
Fixed bug on the display of an erased quiddity of a group.
Fixed refresh of a scenic remote when adding a SIP contact.
Fixed the drag of a shmdata after a hang up of a SIP call.
Fixed bugs on drag&drop.
Fixed bug on duplicated SIP contact in the matrix.
Clean some code in the matrix menus.

Scenic 2.9.16 (2017-03-31)
-------------------------

Fixed bug on exclusive bundle in the matrix menu
Fixed visual bug on the group buttons
Fixed bug when renaming a group
Add the possibility to rename the default Group
Fixed bug on empty shmdata source after opening a saved session
Fixed bug on group button after hang up and recall a SIP call
Fixed bug on mail address when adding a contact
Remove empty default group when hang up a SIP call
Fixed bug when moving a contact to a new group.

Scenic 2.9.15 (2017-03-17)
-------------------------

Fixed bug when receiving several SIP quiddities in the matrix

Scenic 2.9.14 (2017-03-10)
-------------------------

Optimization of the matrix
Fixed synchronization of the matrix with a remote
Fixed some graphic bugs on Ipad
Fixed bugs around the SIP quiddity

Scenic 2.9.13 (2017-02-17)
-------------------------

Fixed crash when loading or reset big file
Optimize property inspector view
Fixed the display of the Button group and preview video

Scenic 2.9.12 (2017-02-06)
-------------------------

Possibility to get properties of a given quiddity in a new tab.
Add a fullscreen mode for the properties Page
Disable the drag&Drop of quiddities when a group is connected.
Fixed the display of the button group for iPad.
Fixed bug on select menu for quiddity properties.

Scenic 2.9.11 (2017-01-20)
-------------------------

Fixed various bugs in matrix connections, properties quiddity, SIP quiddity, shmdata connections.

Scenic 2.9.10 (2017-01-03)
-------------------------

Added the connections between groups

Scenic 2.9.9 (2016-11-28)
-------------------------

Fixed various bugs in touchscreen mode
Fixed various bugs in the edit inspector fo the property of a quiddity
Added shortcuts to manage the dialogs box
When a quiddity is created, it appears directly in the matrix without being prompted for a name
Fixed various bugs in SIP calls
Fixed a bug on the exclusive quiddities when put back in the menu

Scenic 2.9.8 (2016-11-11)
-------------------------

Added the possibility to group in the matrix the sources, destinations, contact or RTP destinations
Groups can be move and rename
Added sub menus to the custom Menus
Added exclusive quiddity to the menus
The name of a quiddity created from a custom menu matchs with the custom menu entry
Fixed various bugs for the SIP connections
Fixed various bugs for the inspector

Scenic 2.9.7 (2016-09-30)
-------------------------

Possibility to import and export scenic save file
Add color picker on the property color of a quiddity
Possibility to grab multiple sources and destinations with the key ctrl and shift
Display the properties in the inspector of a gtkwin window when this one is focused
Various Bugfixes

Scenic 2.9.6 (2016-09-13)
-------------------------

Add the possibility to block a contact form the contact list in the inspector view
Display the FPS
Rework the sources/destinations matrix
Various Bugfixes
Various UI enhancements

Scenic 2.9.5 (2016-08-31)
-------------------------

Added the possibility to rename a quiddity
Sources/Destinations matrix : new icons, improve the visibility of the buttons in touch mode, add custom menus actions ( edit, remove, call, ...) for each quiddities
Fixed bugs in properties inspector

Scenic 2.9.4 (2016-08-17)
-------------------------

Added the possibility to authenticate to scenic when trying to connect from outside
Added enable/disable, create/reset password functionalities for the authentication in Settings
Added the possibility to create custom menus from a config file
Added enable/disable functionalities for sources and destination menus
Added the possibility to rename a saved scenic file
Fixed bugs in the SIP form
Fixed bugs in the dropdown menus in the properties of a quiddity
Fixed a bug that when loading a save file triggers double subscription on the managed quiddities and those created in the node addon

Scenic 2.9.3 (2016-08-05)
-------------------------

Fixed several bugs for the SIP login connection
Add checkbox for the STUN/TURN login
Fixed a bug zith the contact file
Touch events by default
SIP quiditty port is displayed in the advanced SIP login
Fixed a bug that disconnect the thumbnails when creating a new document

Scenic 2.9.2 (2016-07-11)
-------------------------

Fix for possible deadlocks in prop and log callbacks of the node addon

Scenic 2.9.1 (2016-07-11)
-------------------------

Regression - Removed privateQuiddities from the source + destinations menu
Fixed a bug that required multiple clicks to change tabs (page was alternating between normal and empty matrix before switching)
Fixed a bug that prevented the source dummy item from appearing when creating a destination first
Fixed a bug that created duplicate notifications when a client reconnected multiple times to the server
Fixed a bad reference to "self" in server messages code
Restored the full assets copy to the build dir as webpack was not emitting all the used assets.

Scenic 2.9.0 (2016-07-06)
-------------------------

Split SIP login between simple and advanced view
Combined destinations menu
New help page
Better board filtering
Error messages from switcher are now handled in the UI
Various Bugfixes
Various UI enhancements

Scenic 2.8.1 (2016-06-22)
-------------------------

Fixed issue with ShmdataView

Scenic 2.8.0 (2016-06-21)
-------------------------

Merged the 3 matrix/table tabs into one!
Software now properly usable on touch devices
Inspector is now collapsible
Revamped the tooltips system to support future contextual help
In the advanced tab the tree in displayed in the inspector
Server connection timeout reduced to be more reactive to stalls
SIP calls are no properly hung up when creating a new document
Various fixes in the UI

Scenic 2.7.3 (2016-06-08)
-------------------------

Hotfix for jQuery ui widgets not being loaded.

Scenic 2.7.2 (2016-06-08)
-------------------------

Critical bugfix, 2.7.1 was broken.

Scenic 2.7.1 (2016-06-07)
-------------------------

New build process using webpack (see readme for build instructions)

Scenic 2.7.0 (2016-06-01)
-------------------------

New Logo & Icon!!!
Work on mobile too!
Automatically reconnects when connection is dropped or server restarted
Added keyboard shortcuts to the interface (see README)
Added a new "Advanced" tab to edit all hidden quiddities
Inspector is now a fixed panel on the right side of the interface
File loading and SIP login is now done in the new inspector panel
Added row/column highlight when moving mouse on matrix connections
Added possibility to block certain tabs/quiddities from the ui with a config file
Added command line support for providing a defaults file to switcher for automatic quiddity configuration
Added the possibility to create a new document
Added save+save as functionality and file overwrite protection
Properties are now handled through the quiddity information tree
Matrix is now tighter and support more sources destinations without having to scroll
Matrix now groups sources by category
Added userData information tree in quiddities to support scenic-specific configuration (like source/destination order)
Sources and destinations are sortable (though positions are not saved for the moment)
Reduced client/server traffic by caching results of can-sink-caps on the client
SIP is now a "system quiddity" is is no longer created on demand, it is handled by scenic at initialization
Fixed bug that stopped thumbnails updates when closing large video preview
Fixed bug that would leave the property sheet visible after loading a file
Fixed a bug where certain conditions would crash the front end when loading a save file
Various other fixes...

Scenic 2.6.0 (2016-05-03)
-------------------------

Migrated node addon into this repository
Converted properties to use the tree system
Switched to node 5.x
Added quiddity description in the inspector
SOAP (switcher-ctrl) is now disabled by default, see --help to enable it
Fixed bug where loaded document's quiddities didn't get their properties subscribed to
Fixed SIP connection status in the UI when loading a file
Fixed video preview resize in the UI

Scenic 2.5.1 (2016-04-25)
-------------------------

New node addon, directly integrated as an NPM module

Scenic 2.5.0 (2016-??-??)
-------------------------

Added video shmdata previews

Scenic 2.4.1 (2016-03-14)
-------------------------

Minor release fixing various UI issues.

Scenic 2.4.0 (2015-10-23)
-------------------------

This release is a minor release in the stable 2.0 series.

New Features:

* Fraction editor
* Property hierarchy
* Support of switcher new property system

Scenic 2.3.0 (2015-07-23)
-------------------------

This release is a minor release in the stable 2.0 series.

New Features:

* Entire rewrite of the MIDI/Control functionality (property mapping)

Fixes & enhancements:

* Improved session management in the client
* Rate-limited property updates from server
* Property tree improvements

Scenic 2.2.4 (2015-06-22)
-------------------------

This release is a minor release in the stable 2.0 series.

Fixes & enhancements:

* Finished converting network protocol between client and server

Scenic 2.2.3 (2015-06-18)
-------------------------

This release is a hotfix release in the stable 2.0 series.

Fixes & enhancements:

* Chrome process is now detached from the node process
* Pruning all shmdata in a reader or writer tree now correctly notifies the ui

Scenic 2.2.0 (2015-06-15)
-------------------------

This release is a major release in the stable 2.0 series.

New Features

* Shmdata1 (gstreamer1.0) support
* Rewritten core node add-on
* Rewritten network protocol
* Improved localization system
* Visual feedback and notifications (on every connected client) when loading a save file

Fixes & enhancements:

* SIP destinations stay in the destination table across sessions
* Fixed CPU hogging from the CPU meter redraw routine (!)
* Better unit testing
* New make tasks for future packaging

Scenic 2.1.0 (2015-05-12)
------------------------

This release is a major release in the stable 2.0 series.

New Features

* SIP Integration
* Improved UI

Fixed bugs:

* Many, taking over from a previous programmer so I can't list precisely

Scenic 2.0.24 (2014-09-12)
-------------------------

This release is a bugfix release in the stable 2.0 series.

Fixed bugs:

* Fix express path

Scenic 2.0.22 (2014-09-11)
-------------------------

This release is a bugfix release in the stable 2.0 series.

Fixed bugs:

* path issues introduced in previous release

Scenic 2.0.18 (2014-09-10)
-------------------------

This release is a developer snapshot in the stable 2.0 series.

New features:

* Filter display of shmdatas
* Display CPU and network usage in UI's header
* Show only pertinent information when creating jack sinks/sources

Fixed bugs:

* Source name with fakesink not correct under certain conditions
* Scenic would not start without network
* Various fixes to mouse click handling
* Auto selecting value when editing properties
* Creation of 'undefined' when clicking on source/network
* Hiding (dis)connect when configuring audio destination
* Preview failing when httpsdpdec would take a long time
* Dangling jack inputs after removal
* some sources would not become available under sink tab

Known bugs:

* CPU meter displays +1 CPU which is a total average (first meter)
* There is no indication of the actual network bandwidth consumption only a relative amount capped at 15MBps

Scenic 2.0.16 (2014-06-10)
-------------------------

This a bug-fix release developer snapshot in the stable 2.0 series.

Fixed bugs:

* Fix installation paths
* Handle version lookup with -v or --version even if optimist not present
* Prepare for proper handling by debian packaging system

Known bugs:

* Automatic launch of Scenic following the modules installation may fail. Quitting the chrome application and relaunching scenic works

Scenic 2.0.12 (2014-04-10)
-------------------------

This a bug-fix release developer snapshot in the stable 2.0 series.

Fixe bugs:

* Fix installation paths
* Handle version lookup with -v or --version even if optimist not present

Known bugs:

* If install script was triggered due to missing node modules, the automatic launch of Scenic following the modules installation fails. Quitting the chro
me application and relaunching scenic works

Scenic 2.0.8 (2014-04-01)
-------------------------

This release is a developer snapshot in the stable 2.0 series.

New features:

* Save and load scenes
* Connect audio sources and sinks to jack server
* Check and install missing dependencies automatically
* Install nodejs modules locally for each user

Known bugs:

* Sometimes the engine crashes when rapidly creating and distroying quiddities, including when loading from saved file

Scenic 0.2.0 (2013-10-15)
-------------------------

This release is a developer snapshot in the stable 0.4 series.

New features:

* Automatically detect audio/video sources
* Control quiddities properties dynamically
* Support MIDI
* MIDI learn
* Audio video previews (local and remote)

Known bugs:

* Sometimes engine hangs after multiple opening/closing of preview window
* No obvious error messages when communication with remote host is not possible

Scenic 0.2.0 (2013-07-05)
-------------------------

This release is a developer snapshot in the stable 0.2 series.

New features:

* Use The functionnality of switcher 2.0 with an interface web

Known bugs:

* scenic sometimes leaves completely at server start
