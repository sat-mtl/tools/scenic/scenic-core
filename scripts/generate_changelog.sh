#!/bin/bash

# Check arguments
if [ "$#" -ne 1 ]; then
  echo "VERSION argument is required"
  exit 0
fi

function filter_commit () {
  grep -Ev "merge|[rR]elease"
}

function trim_commit () {
  grep -v "^[[:space:]]*$" | sed 's/ \+/ /g'
}

function parse_date () {
  date --date="${1}" "+%Y-%m-%d"
}

function to_markdown_h2 () {
  sed -e 's/$/\n----------------------------------\n /'
}

function to_markdown_li () {
  sed -e 's/^/\* /'
}

function fetch_commits_from () {
  git log \
    --first-parent \
    --pretty=%b \
    --decorate \
    --since="${1}" \
    | filter_commit \
    | trim_commit
}

LAST_GIT_TAG="$(git describe --tags `git rev-list --tags --max-count=1`)"
LAST_GIT_DATE="$(git log -1 --format=%ai ${LAST_GIT_TAG})"

CHANGELOG_VERSION="${1:-0.0.0}"
CHANGELOG_DATE="$(date "+%Y-%m-%d")"
CHANGLOG_TITLE="$(echo Scenic Core ${CHANGELOG_VERSION} \(${CHANGELOG_DATE}\) | to_markdown_h2)"
CHANGELOG_BODY="$(fetch_commits_from ${LAST_GIT_DATE} | to_markdown_li)"

echo -e "${CHANGLOG_TITLE}\n${CHANGELOG_BODY}"
