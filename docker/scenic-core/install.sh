#!/bin/bash

# exit on error
set -e

# Log that we are running in custom GStreamer SDK
echo "Using GSTREAMER_ROOT=$GSTREAMER_ROOT"

apt-get update -y

# Install build tools
apt-get install -y -qq --no-install-recommends \
    ca-certificates \
    git \
    cmake \
    ninja-build \
    build-essential \
    pkg-config

# Install Shmdata build dependencies
apt-get install -y -qq --no-install-recommends \
    python3-dev

# Install Shmdata binaries
ln -s /build/opt/shmdata /opt/shmdata
cd /opt/shmdata
ninja install -C build
ldconfig
cd /opt
rm /opt/shmdata

# Remove Shmdata build dependencies
apt-get remove -y \
    python3-dev

# Install ndi2shmdata  build and runtime dependencies
apt-get install -y -qq --no-install-recommends \
    libavahi-client-dev \
    libavahi-client3

# Install ndi2shmdata binaries
ln -s /build/opt/ndi2shmdata /opt/ndi2shmdata
cd /opt/ndi2shmdata
ninja install -C build
ldconfig
cd /opt
rm /opt/ndi2shmdata

# Remove ndi2shmdata build dependencies
apt-get remove -y \
    libavahi-client-dev

# Install Switcher build dependencies
apt-get install -y -qq \
    build-essential \
    cmake \
    git \
    libcurl4-gnutls-dev \
    libjack-jackd2-dev \
    libjson-glib-dev \
    liblo-dev \
    libportmidi-dev \
    libpulse-dev \
    libsamplerate0-dev \
    libsoup2.4-dev \
    libspdlog-dev \
    libssl-dev \
    libxcursor-dev \
    libxi-dev \
    libxinerama-dev \
    libxrandr-dev \
    libxxf86vm-dev \
    python3-dev \
    swh-plugins \
    uuid-dev \
    libfmt-dev \
    libgl-dev

# Install Switcher
ln -s /build/opt/switcher /opt/switcher
cd /opt/switcher
ninja install -C build
ldconfig
cd /opt
rm /opt/switcher

# Install Switcher Pipesplint
ln -s /build/opt/switcher-pipesplint /opt/switcher-pipesplint
cd /opt/switcher-pipesplint
ninja install -C build
cd /opt
rm /opt/switcher-pipesplint

# Remove Switcher build dependencies
apt-get remove -y \
    build-essential \
    cmake \
    git \
    libcurl4-gnutls-dev \
    libjack-jackd2-dev \
    libjson-glib-dev \
    liblo-dev \
    libportmidi-dev \
    libpulse-dev \
    libsamplerate0-dev \
    libsoup2.4-dev \
    libspdlog-dev \
    libssl-dev \
    libxcursor-dev \
    libxi-dev \
    libxinerama-dev \
    libxrandr-dev \
    libxxf86vm-dev \
    python3-dev \
    swh-plugins \
    uuid-dev \
    libfmt-dev \
    libgl-dev

# Add mesa-utils package to get glxgears test command,
# it also appears to install required pacakges for opengl applications 
# to work inside Docker image (such as libglx/libgl/libllvm)
apt-get install -y -qq --no-install-recommends \
    mesa-utils

# Remove build tools
apt-get remove -y \
    ca-certificates \
    git \
    cmake \
    ninja-build \
    build-essential \
    pkg-config

# Install GDB
apt-get install -y -qq \
    gdb

# Install rswebrtc
cp /build/opt/gst-plugins-rs/target/release/libgstrswebrtc.so /opt/gstreamer-scenic/lib/x86_64-linux-gnu/gstreamer-1.0/libgstrswebrtc.so
gst-inspect-1.0 --no-colors rswebrtc