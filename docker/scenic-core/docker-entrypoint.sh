#!/bin/bash

USER=scenic
PUID=${PUID:-1000}
PGID=${PGID:-1000}
groupmod -o -g "$PGID" $USER
usermod -o -u "$PUID" $USER

DEBUG=${DEBUG:-on}
DEBUG_SIO=${DEBUG_SIO:-off}
PORT=${PORT:-8000}

#######################################
# Clean stray shmdata paths
#######################################
cleanShmdataPaths () {
    rm -rf /tmp/scenic
}

#######################################
# Create 32 video4linux2 devices nodes
# allow for dynamic video devices passthrough when started with 
# --device-cgroup-rule 'c 81:* rmw' option
#######################################
createVideoDevices () {
    for i in {0..32}
    do
        mknod -m g=rw,o-rw /dev/video$i c 81 $i
        chgrp video /dev/video$i
    done
}

cleanShmdataPaths
createVideoDevices

if [ $DEBUG == "on" ]; then DEBUG_ARGUMENT="--debug"; fi
if [ $DEBUG_SIO == "on" ]; then DEBUG_SIO_ARGUMENT="--debug-sio"; fi

export GSTREAMER_ROOT="/opt/gstreamer-scenic"
export PATH="${GSTREAMER_ROOT}/bin${PATH:+:$PATH}"
export LD_LIBRARY_PATH="${GSTREAMER_ROOT}/lib/x86_64-linux-gnu${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
export XDG_DATA_DIRS="/usr/share:${GSTREAMER_ROOT}/share${XDG_DATA_DIRS:+:$XDG_DATA_DIRS}"
export XDG_CONFIG_DIRS="${GSTREAMER_ROOT}/etc/xdg${XDG_CONFIG_DIRS:+:$XDG_CONFIG_DIRS}"
export GST_REGISTRY_1_0="${HOME}/.cache/gstreamer-1.0/gstreamer-cerbero-registry"
export GST_PLUGIN_SCANNER_1_0="${GSTREAMER_ROOT}/libexec/gstreamer-1.0/gst-plugin-scanner"
export GST_PLUGIN_PATH_1_0="${GSTREAMER_ROOT}/lib/x86_64-linux-gnu/gstreamer-1.0"
export GST_PLUGIN_SYSTEM_PATH_1_0="${GSTREAMER_ROOT}/lib/x86_64-linux-gnu/gstreamer-1.0"
export PYTHONPATH="${GSTREAMER_ROOT}/lib/python3.10/site-packages${PYTHONPATH:+:$PYTHONPATH}"
export GIO_EXTRA_MODULES="${GSTREAMER_ROOT}/lib/x86_64-linux-gnu/gio/modules"
export GI_TYPELIB_PATH="${GSTREAMER_ROOT}/lib/x86_64-linux-gnu/girepository-1.0"

exec su "$USER" -c "/usr/local/bin/switcher $DEBUG_ARGUMENT $DEBUG_SIO_ARGUMENT --port $PORT"
