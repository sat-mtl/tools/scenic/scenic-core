#!/bin/bash

# exit on error
set -e

cd /opt/cerbero
./cerbero-uninstalled \
        -v alsa \
        -v gi \
        -v jack \
        -v pulse \
        -v python \
        -v rust \
        -v v4l2 \
        -v x11 \
        -v vaapi \
        package gstreamer-1.0-scenic
        
ls -alh gstreamer-1.0-scenic-linux-x86_64-${CERBERO_TAG}.tar.xz
ls -alh gstreamer-1.0-scenic-linux-x86_64-${CERBERO_TAG}-devel.tar.xz
