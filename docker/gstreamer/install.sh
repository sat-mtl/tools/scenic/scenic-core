#!/bin/bash

# exit on error
set -e

# extract runtime package
mkdir -p /opt/gstreamer-scenic
tar xvf /opt/cerbero/gstreamer-1.0-scenic-linux-x86_64-${CERBERO_TAG}.tar.xz --directory=/opt/gstreamer-scenic

# workaround gst-shell script generation regression starting with 1.24.1
cp /gst-shell /opt/gstreamer-scenic/bin/gst-shell

# Change gst-shell GSTREAMER_ROOT environment variable to point to /opt/gstreamer-scenic
sed -i 's/\/opt\/cerbero\/build\/dist\/linux_x86_64/\/opt\/gstreamer-scenic/' /opt/gstreamer-scenic/bin/gst-shell

# Fix missing /usr/share/glib-2.0/schemas/gschemas.compiled file by adding system XDG_DATA_DIRS path
# to fix uridecodebin element erroring on :
#   (gst-launch-1.0:39): GLib-GIO-ERROR **: 11:56:52.884: No GSettings schemas are installed on the system
sed -i 's/XDG_DATA_DIRS=\"/XDG_DATA_DIRS=\"\/usr\/share:/' /opt/gstreamer-scenic/bin/gst-shell

# Disable libmpg123 to fix symbol error while loading libgstpulseaudio.so
#   (gst-plugin-scanner:96076): GStreamer-WARNING **: 12:47:32.964: Failed to load plugin '/opt/gstreamer-scenic-devel/lib/x86_64-linux-gnu/gstreamer-1.0/libgstpulseaudio.so': /lib/x86_64-linux-gnu/libsndfile.so.1: undefined symbol: mpg123_info2
mv /opt/gstreamer-scenic/lib/x86_64-linux-gnu/libmpg123.so.0 /opt/gstreamer-scenic/lib/x86_64-linux-gnu/libmpg123.so.0.disabled

# extract devel package
mkdir -p /opt/gstreamer-scenic-devel 
tar xvf /opt/cerbero/gstreamer-1.0-scenic-linux-x86_64-${CERBERO_TAG}.tar.xz --directory=/opt/gstreamer-scenic-devel
tar xvf /opt/cerbero/gstreamer-1.0-scenic-linux-x86_64-${CERBERO_TAG}-devel.tar.xz --directory=/opt/gstreamer-scenic-devel

# workaround gst-shell script generation regression starting with 1.24.1
cp /gst-shell /opt/gstreamer-scenic-devel/bin/gst-shell

# Change gst-shell GSTREAMER_ROOT environment variable to point to /opt/gstreamer-scenic-devel
sed -i 's/\/opt\/cerbero\/build\/dist\/linux_x86_64/\/opt\/gstreamer-scenic-devel/' /opt/gstreamer-scenic-devel/bin/gst-shell

# Fix missing /usr/share/glib-2.0/schemas/gschemas.compiled file by adding system XDG_DATA_DIRS path
# to fix uridecodebin element erroring on :
#   (gst-launch-1.0:39): GLib-GIO-ERROR **: 11:56:52.884: No GSettings schemas are installed on the system
sed -i 's/XDG_DATA_DIRS=\"/XDG_DATA_DIRS=\"\/usr\/share:/' /opt/gstreamer-scenic-devel/bin/gst-shell

# Disable libmpg123 to fix symbol error while loading libgstpulseaudio.so
#   (gst-plugin-scanner:96076): GStreamer-WARNING **: 12:47:32.964: Failed to load plugin '/opt/gstreamer-scenic-devel/lib/x86_64-linux-gnu/gstreamer-1.0/libgstpulseaudio.so': /lib/x86_64-linux-gnu/libsndfile.so.1: undefined symbol: mpg123_info2
mv /opt/gstreamer-scenic-devel/lib/x86_64-linux-gnu/libmpg123.so.0 /opt/gstreamer-scenic-devel/lib/x86_64-linux-gnu/libmpg123.so.0.disabled