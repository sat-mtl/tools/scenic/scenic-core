#!/bin/bash

# exit on error
set -e

# Install Switcher runtime dependencies
apt-get install -y -qq \
    jackd2 \
    libcurl4 \
    libjson-glib-1.0-0 \
    liblo7 \
    libportmidi0 \
    libpulse-mainloop-glib0 \
    libpython3.10 \
    libsamplerate0 \
    libsoup2.4-1 \
    libspdlog1 \
    libssl3 \
    libxcursor1 \
    libxi6 \
    libxinerama1 \
    libxrandr2 \
    libxxf86vm1 \
    pulseaudio \
    python3 \
    uuid \
    libxv1

# Install pipx package manager for switcherio
apt-get install -y -qq --no-install-recommends \
    python3-pip \
    python3-venv
python3 -m pip install --user pipx
python3 -m pipx ensurepath
