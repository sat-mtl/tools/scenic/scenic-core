#!/bin/bash

# exit on error
set -e

# Install ndi2shmdata runtime dependencies
apt-get install -y -qq --no-install-recommends \
    libavahi-client3
