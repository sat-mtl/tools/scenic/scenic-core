#!/bin/bash

# exit on error
set -e

# Install Shmdata dependencies
apt-get install -y -qq --no-install-recommends \
    ca-certificates \
    git \
    cmake \
    ninja-build \
    build-essential \
    python3-dev \
    pkg-config
