#!/bin/bash

# exit on error
set -e

# Install toolchain
apt-get install -y -qq --no-install-recommends \
    curl \
    ca-certificates \
    build-essential \
    pkg-config \
    libssl-dev
curl https://sh.rustup.rs -sSf | sh -s -- -y
source "$HOME/.cargo/env"
cargo --version
rustc --version
