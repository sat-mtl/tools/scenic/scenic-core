#!/bin/bash

# exit on error
set -e

# Log that we are running in custom GStreamer SDK
echo "Using GSTREAMER_ROOT=$GSTREAMER_ROOT"

# Build ndi2shmdata
cd /opt/ndi2shmdata
cmake -GNinja -B build \
    -DACCEPT_NDI_LICENSE=ON \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo
ninja -C build
ninja install -C build
ldconfig
