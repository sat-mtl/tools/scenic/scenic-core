#!/bin/bash

# exit on error
set -e

# Fetch ndi2shmdata sources
cd /opt
git clone -b $NDI2SHMDATA_TAG https://gitlab.com/sat-mtl/tools/ndi2shmdata.git
