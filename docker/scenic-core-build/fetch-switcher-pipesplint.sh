#!/bin/bash

# exit on error
set -e

# Fetch Switcher Pipesplint sources
cd /opt
git clone -b $SWITCHER_PIPESPLINT_TAG https://gitlab.com/ogauthier_sat/switcher-pipesplint.git
