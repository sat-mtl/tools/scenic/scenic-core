#!/bin/bash

# exit on error
set -e

# Fetch Shmdata sources
cd /opt
git clone -b $SHMDATA_TAG https://gitlab.com/sat-mtl/tools/shmdata.git
