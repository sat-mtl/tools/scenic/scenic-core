#!/bin/bash

# exit on error
set -e

# Fetch gst-plugins-rs sources
cd /opt
git clone -b $GST_PLUGINS_RS_TAG $GST_PLUGINS_RS_GIT_URL
cd gst-plugins-rs
git checkout $GST_PLUGINS_RS_GIT_COMMIT
