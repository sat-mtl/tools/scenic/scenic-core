#!/bin/bash

# exit on error
set -e

# Log that we are running in custom GStreamer SDK
echo "Using GSTREAMER_ROOT=$GSTREAMER_ROOT"

# Build rswebrtc
cd /opt/gst-plugins-rs/net/webrtc
source "$HOME/.cargo/env"
cargo build --release

# Install rswebrtc
cp /opt/gst-plugins-rs/target/release/libgstrswebrtc.so /opt/gstreamer-scenic-devel/lib/x86_64-linux-gnu/gstreamer-1.0/libgstrswebrtc.so
gst-inspect-1.0 --no-colors rswebrtc